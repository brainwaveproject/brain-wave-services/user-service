## Create Brain Wave user Database

1. Launch `1_create_schema.sql` script to create schema.

2. Launch `2_create_user_table.sql` script to create user table.