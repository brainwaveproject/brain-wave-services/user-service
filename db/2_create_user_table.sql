CREATE TABLE "user" (
	id serial NOT NULL,
	username varchar(255) NOT NULL UNIQUE,
	"password" varchar(255) NOT NULL,
	CONSTRAINT user_pkey PRIMARY KEY (id)
);