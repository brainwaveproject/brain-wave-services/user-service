package com.florian935.brainwave.user.controller;

import com.florian935.brainwave.user.domain.User;
import com.florian935.brainwave.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@RestController
@RequestMapping("/api/v1.0/users")
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class UserController {

    UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> getAll() {

        final List<User> users = userService.getAll();

        return ResponseEntity.ok(users);
    }

    @PostMapping
    public ResponseEntity<User> save(@RequestBody User user) {

        final User userToSave = userService.save(user);

        return ResponseEntity.ok(userToSave);
    }
}
