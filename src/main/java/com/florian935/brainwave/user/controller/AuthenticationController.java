package com.florian935.brainwave.user.controller;

import com.florian935.brainwave.user.domain.JwtResponse;
import com.florian935.brainwave.user.domain.User;
import com.florian935.brainwave.user.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@RestController
@RequestMapping("/api/v1.0/authenticate")
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class AuthenticationController {

    AuthenticationService authenticationService;

    @PostMapping
    public ResponseEntity<JwtResponse> authenticate(@RequestBody User user) {

        final JwtResponse jwtResponse = authenticationService.authenticate(user);

        return ResponseEntity.ok(jwtResponse);
    }
}
