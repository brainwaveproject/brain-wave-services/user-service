package com.florian935.brainwave.user.config;


import com.florian935.brainwave.user.security.jwt.JwtRequestFilter;
import com.florian935.brainwave.user.security.jwt.JwtAuthenticationEntryPoint;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static lombok.AccessLevel.PRIVATE;
import static org.springframework.http.HttpMethod.POST;

/**
 * @author florian935
 */
@EnableWebSecurity
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    JwtRequestFilter jwtRequestFilter;
    JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {

        return authenticationManager();
    }

    protected void configure(HttpSecurity httpSecurity) throws Exception {
        final String ACTUATOR_URI = "/actuator/**";
        final String USERS_URI = "/api/v1.0/users";
        final String AUTHENTICATION_URI = "/api/v1.0/authenticate";

        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(ACTUATOR_URI).permitAll()
                .antMatchers(POST, USERS_URI).permitAll()
                .antMatchers(POST, AUTHENTICATION_URI).permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
        ;
    }
}