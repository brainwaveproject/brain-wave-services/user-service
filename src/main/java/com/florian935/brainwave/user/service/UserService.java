package com.florian935.brainwave.user.service;

import com.florian935.brainwave.user.domain.User;

import java.util.List;

/**
 * @author florian935
 */
public interface UserService {

    List<User> getAll();

    User save(User user);
}
