package com.florian935.brainwave.user.service;

import com.florian935.brainwave.user.domain.User;
import com.florian935.brainwave.user.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class UserServiceImpl implements UserService {

    UserRepository userRepository;
    PasswordEncoder passwordEncoder;

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User save(User user) {
        final User userToSave = User
                .builder()
                    .username(user.getUsername())
                    .password(passwordEncoder.encode(user.getPassword()))
                .build();

        return userRepository.save(userToSave);
    }
}
