package com.florian935.brainwave.user.service;

import com.florian935.brainwave.user.domain.JwtResponse;
import com.florian935.brainwave.user.domain.User;

/**
 * @author florian935
 */
public interface AuthenticationService {

    JwtResponse authenticate(User user);
}
