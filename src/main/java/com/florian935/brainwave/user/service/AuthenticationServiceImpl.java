package com.florian935.brainwave.user.service;

import com.florian935.brainwave.user.domain.JwtResponse;
import com.florian935.brainwave.user.domain.User;
import com.florian935.brainwave.user.security.jwt.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class AuthenticationServiceImpl implements AuthenticationService {

    AuthenticationManager authenticationManager;
    UserDetailsService userDetailsService;
    JwtTokenUtil jwtTokenUtil;

    @Override
    public JwtResponse authenticate(User user) {

        final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(
                        user.getUsername(), user.getPassword()
                );
        authenticationManager.authenticate(usernamePasswordAuthenticationToken);

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(user.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return new JwtResponse(token);
    }
}
