package com.florian935.brainwave.user.security.jwt;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

/**
 * @author florian935
 */
@Component
@Slf4j
@FieldDefaults(level = PRIVATE)
public class JwtTokenUtil {

    static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    @Value("${jwt.secret}")
    private String secret;

    public String getUsernameFromJwtToken(String token) {

        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    public String generateToken(UserDetails userDetails) {

        final Map<String, Object> claims = new HashMap<>();

        return doGenerateToken(claims, userDetails.getUsername());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public boolean validateJwtToken(String authToken) {

        try {
            parseToken(authToken);
            return true;
        } catch (Exception e) {
            logError(e);
            return false;
        }
    }

    private void parseToken(String authToken) {

        Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
    }

    private void logError(Exception exception) {

        log.error("Invalid JWT: {}", exception.getMessage());
    }
}
